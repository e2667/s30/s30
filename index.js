const express = require("express");
const mongoose = require("mongoose");
//Mongoose is a package that allows creation of Schemas to model our data structure.
//Also has access to a number of methods for manipulating our database

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@course-booking.4kkyq.mongodb.net/B157_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Set notifications for connection success or failure
//Connection to the database
//Allows us to handle errors when the initial connection is established
let db = mongoose.connection;

//If a connection error occurred, output in the console.
db.on("error", console.error.bind(console, "Connection error"));

//If the connection is successful, output in the console.
db.once("open", () => console.log("We're connected to the cloud database"));

//Schemas determine the structure of the documents to be written in the database.
//This acts as a blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

//Model uses schemas and are used to create/instantiate objects that corresponds to the schema
//Models must be in a singular form and capitalized.
//The first parameter of the mongoose model method indicates the collection in where will be stored in the mongoDB collection/
//The second parameter is used to specify the schema/blueprint of the documents.
//Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into plural form when creating a collection.
const Task = mongoose.model("Task", taskSchema);

//Setup for allowing the server to handle data from requests
//Allows our app to read json data
app.use(express.json());

//Allows our app to read data from forms
app.use(express.urlencoded({extended: true}));

//Creating a new Task
/*
	1. Add a functionality to check if there is are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the tasks doesn't exist, we add it in our database.
	2. The task data will be coming from the request's body.
	3. Create a Task object with a "name" field/property.
*/

app.post("/tasks", (req,res) => {
	Task.findOne({name: req.body.name}, (err,result) => {
		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task created.")
				}
			})
		}
	})
});

//GET request to retrieve all the documents.
/*
	1. Retrieve all the documents
	2. IF an error is encountered, print the error
	3. If no errors are found, send a success back to the client
*/

app.get("/tasks", (req,res) => {
	Task.find({}, (err, result) => {
		if(err){
			console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})

app.get('/hello', (req,res) => {
	res.send("Hello World")
});

const userSchema = new mongoose.Schema({
	name: String,
	password: String
});
const User = mongoose.model("User", userSchema);
app.post("/signup", (req,res) => {
	User.findOne({name: req.body.name}, (err,result) => {
		if (result != null && result.name == req.body.name){
			return res.send("User already exist");
		} else {
			let newUser = new User({
				name: req.body.name,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New user registered")
				}
			})
		}
	})
});


app.listen(port,()=>console.log(`Server is running at port ${port}`));